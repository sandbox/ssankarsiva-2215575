Registration mail domain validation:
------------------
Author and mantainer: Sivasankar
Requirements:         Drupal 7
License:              GPL (see LICENSE.txt)

Introduction
------------
This module is purposed to validating registering user mail and company domain must be same 


Installation and configuration
-------------------------------
  
1) Enable this module as drupal standard procedure

2) Go to 'admin/config/email_domain' and set disallowed domains as comma seperated lists

3) It can be accessed who have adminiter filter permisiion
